from decimal import Decimal as D
from functools import partial

from taxes import calc_profit, process_revo


def test_process_revo():
    # fmt: off
    trades = [
        # test costs
        {"date": "2023-12-24", "time": "7:07", "t_type": "Cost", "buy": None, "sell": "RON", "USD": "1", "fee_%": None},
        {"date": "2023-12-25", "time": "7:07", "t_type": "Cost", "buy": None, "sell": "RON", "USD": "2", "fee_%": None},
        {"date": "2023-12-26", "time": "7:07", "t_type": "Cost", "buy": None, "sell": "RON", "USD": "3", "fee_%": None},
        # test that we sort correctly before any calculations
        {"date": "2023-12-25", "time": "7:11", "t_type": "Sell", "buy": "RON", "sell": "DOT", "sell_Q": "100", "USD": "11"},
        {"date": "2023-11-24", "time": "7:09", "t_type": "Sell", "buy": "RON", "sell": "DOT", "sell_Q": "50", "USD": "10"},
        {"date": "2023-10-24", "time": "7:08", "t_type": "Buy", "buy": "DOT", "buy_Q": "100", "sell": "RON", "USD": "12", "fee_%": "5.0"},
        {"date": "2023-12-24", "time": "7:10", "t_type": "Buy", "buy": "DOT", "buy_Q": "100", "sell": "RON", "USD": "10", "fee_%": "5.0"},
        # test a mix of buys, rewards, and sells
        {"date": "2023-12-24", "time": "07:00", "t_type": "Buy", "buy": "ETH", "buy_Q": "3", "sell": "RON", "USD": "30", "fee_%": "5.0"},
        {"date": "2023-12-24", "time": "08:00", "t_type": "Reward", "buy": "ETH", "buy_Q": "1", "sell": "RON", "USD": "10", "fee_%": None},
        {"date": "2023-12-24", "time": "09:00", "t_type": "Sell", "buy": "RON", "sell": "ETH", "sell_Q": "2", "USD": "55"},
        {"date": "2023-12-24", "time": "10:00", "t_type": "Buy", "buy": "ETH", "buy_Q": "2", "sell": "RON", "USD": "20", "fee_%": "5.0"},
        {"date": "2023-12-24", "time": "11:00", "t_type": "Sell", "buy": "RON", "sell": "ETH", "sell_Q": "1", "USD": "25"},
        {"date": "2023-12-24", "time": "12:00", "t_type": "Sell", "buy": "RON", "sell": "ETH", "sell_Q": "2", "USD": "30"},
        {"date": "2023-12-24", "time": "13:00", "t_type": "Reward", "buy": "ETH", "buy_Q": "2", "sell": "RON", "USD": "35", "fee_%": None},
        {"date": "2023-12-24", "time": "14:00", "t_type": "Reward", "buy": "ETH", "buy_Q": "2", "sell": "RON", "USD": "40", "fee_%": None},
    ]
    # fmt: on

    rates = {
        "USD": {
            "2023-10-24": 1,
            "2023-11-24": 2,
            "2023-12-24": 3,
            "2023-12-25": 4,
            "2023-12-26": 5,
        }
    }

    # Buys include rewards because we need to use them when we calculate the net profit
    # of the sell trades. The order should be maintained for buys inter-weaved with rewards
    processed = process_revo(trades, rates)
    assert processed["__REWARDS__"] == {"ETH": [D(30), D(105), D(120)]}

    assert processed["__COSTS__"] == [(D(0), D(3)), (D(0), D(8)), (D(0), D(15))]
    assert processed["DOT"] == [
        (D(100), D("-12.63157894736842105263157895")),
        (D(-50), D(20)),
        (D(100), D("-31.57894736842105263157894737")),
        (D(-100), D(44)),
    ]
    assert processed["ETH"] == [
        (D(3), D("-94.73684210526315789473684211")),
        (D(1), D(30)),
        (D(-2), D(165)),
        (D(2), D("-63.15789473684210526315789474")),
        (D(-1), D(75)),
        (D(-2), D(90)),
        (D(2), D(105)),
        (D(2), D(120)),
    ]


def test_calc_profit():
    trades = {
        "__COSTS__": [(D(0), D(3)), (D(0), D(8)), (D(0), D(15))],
        "DOT": [
            (D(100), D("-12.63157894736842105263157895")),
            (D(-50), D(20)),
            (D(100), D("-31.57894736842105263157894737")),
            (D(-100), D(44)),
        ],
        "ETH": [
            (D(3), D("-94.73684210526315789473684211")),
            (D(1), D(-30)),
            (D(-2), D(165)),
            (D(2), D("-63.15789473684210526315789474")),
            (D(-1), D(75)),
            (D(-2), D(90)),
            (D(2), D(-105)),
            (D(2), D(-120)),
        ],
    }

    actual = calc_profit(trades)

    expected = {
        "__TOTAL__": D("183.2631578947368421052631579"),
        "__COSTS__": D(-26),
        "DOT": D("35.57894736842105263157894737"),
        "ETH": D("173.6842105263157894736842105"),
    }

    # The default precision of decimals is 28 significant digits. The rounding will
    # ensure that the results are the same up to the 20th decimal place.
    round_10 = partial(round, ndigits=20)
    actual = {k: round_10(v) for k, v in actual.items()}
    expected = {k: round_10(v) for k, v in expected.items()}

    assert actual == expected

