import re
import subprocess
from sys import argv


def main():
    errors = []
    ERROR = "[\033[91mERROR\033[0m]"

    # audio playlist: youtube-dl --download-archive downloaded.txt -ci --no-warnings -x --audio-format mp3 -f bestaudio -o '%(playlist)s/%(title)s.%(ext)s'
    # video playlist: youtube-dl --download-archive downloaded.txt -ci --no-warnings -x --audio-format mp3 -f bestaudio -o '%(playlist)s/%(playlist_index)s.%(title)s.%(ext)s'
    # audio: youtube-dl -x --audio-format mp3 -f bestaudio '-o %(title)s.%(ext)s'
    # video: youtube-dl -f \\"bestvideo[height<=720]+bestaudio/best[height<=720]\\" '-o %(title)s.%(ext)s'
    base = "yt-dlp -i --no-warnings --cookies-from-browser firefox"
    output = {
        "music": "-x --audio-format mp3 -f bestaudio",
        "podcast": "-x --audio-format mp3 -f bestaudio",
        "video": "-f 'bestvideo[height<=720]+bestaudio/best[height<=720]'",
    }

    filename = {
        True: {
            "music": "-o '%(playlist)s/%(uploader)s - %(title)s.%(ext)s'",
            "podcast": "-o '%(playlist)s/%(playlist_index)s - %(uploader)s - %(title)s - %(id)s.%(ext)s'",
            "video": "-o '%(playlist)s/%(playlist_index)s - %(uploader)s - %(playlist_index)s - %(title)s - %(id)s.%(ext)s'",
        },
        False: {
            "music": "-o '%(uploader)s - %(title)s.%(ext)s'",
            "podcast": "-o '%(uploader)s - %(title)s - %(id)s.%(ext)s'",
            "video": "-o '%(uploader)s - %(title)s - %(id)s.%(ext)s'",
        },
    }

    if len(argv) < 3:
        print(f"{ERROR} Not enough data")
        return

    save_as = argv[1]
    if save_as not in output:
        print(f"{ERROR} Output format unrecognized")
        return

    urls = argv[2:]
    for url in urls:
        # generate the command
        url = parse_url(url=url)
        get_playlist_name = f"{base} -o '%(playlist)s' -s --get-filename --playlist-items 1 {url}".split()
        playlist = subprocess.run(get_playlist_name, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        playlist = playlist.stdout.decode("utf-8")

        if "ERROR" in playlist:
            errors.append({"cmd": get_playlist_name, "url": url, "error": playlist})
            continue

        if ("'NA'\n" in playlist) or ("ytsearch:" in url and len(url) == 22):
            is_playlist = False
        else:
            is_playlist = True

        command = f"{base} {output[save_as]} {filename[is_playlist][save_as]} {url}"
        # print(is_playlist, playlist)
        # print(command)
        # return

        # run the command
        error = subprocess.run(command, shell=True, stderr=subprocess.PIPE).stderr.decode("utf-8")
        if "ERROR" in error:
            errors.append(
                {
                    "command": command,
                    "error": error,
                    "is_playlist": is_playlist,
                    "save_as": save_as,
                    "url": url,
                }
            )

    for error in errors:
        for k, v in error.items():
            print(k, "=>", v)
        print("-" * 80 + "\n")


def parse_url(*, url, **kwargs):
    if url.startswith("https://www.youtube.com/"):
        if "list=" in url:
            return re.search(r"(\?|&)list=([^&]*)", url).group(2)
        elif url.startswith("https://www.youtube.com/embed/"):
            url = re.search(r"embed\/([-a-zA-Z0-9]+)", url).group(1)
            return f"https://www.youtube.com/?v={url}"
        else:
            url = re.search(r"\?v=([^&]*)", url).group(1)
            return f"https://www.youtube.com/?v={url}"
    elif url.startswith("https://youtu.be/"):
        if "list=WL" in url:
            url = re.search(r"https://youtu.be/([^/$]*)\?list=WL", url).group(1)
            return f"https://www.youtube.com/?v={url}"
        elif "list=" in url:
            return re.search(r"(\?|&)list=([^&]*)", url).group(2)
        else:
            url = re.search(r"https://youtu.be/([^/$]*)", url).group(1)
            return f"https://www.youtube.com/?v={url}"
    elif url.startswith("-"):
        return f"https://www.youtube.com/?v={url}"
    else:
        return url


def unit_test():
    urls = {
        "https://www.youtube.com/watch?v=UO_84C3fpuI&list=PLenUrOlreSp6EXV4PJWLEvLIdnacjn-2w&index=4": "PLenUrOlreSp6EXV4PJWLEvLIdnacjn-2w",
        "https://www.youtube.com/watch?v=UO_84C3fpuI&list=PLenUrOlreSp6EXV4PJWLEvLIdnacjn-2w": "PLenUrOlreSp6EXV4PJWLEvLIdnacjn-2w",
        "https://www.youtube.com/playlist?list=PLenUrOlreSp6EXV4PJWLEvLIdnacjn-2w": "PLenUrOlreSp6EXV4PJWLEvLIdnacjn-2w",
        "https://youtu.be/xnKhsTXoKCI?list=PLenUrOlreSp6EXV4PJWLEvLIdnacjn-2w": "PLenUrOlreSp6EXV4PJWLEvLIdnacjn-2w",
        "https://www.youtube.com/watch?v=UO_84C3fpuI": "UO_84C3fpuI",
        "https://youtu.be/Wy9q22isx3U": "Wy9q22isx3U",
    }

    for raw_url, correct_url in urls.items():
        parsed_url = parse_url(url=raw_url)
        if correct_url != parsed_url:
            print(f"raw: {raw_url}")
            print(f"expected: {correct_url}")
            print(f"received: {parsed_url}")
            print()


if __name__ == "__main__":
    main()
