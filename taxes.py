"""
We use Decimal everywhere to avoid rounding errors. The default precision is 28
significant digits, which is more than enough. E.g., 123.45 has 5 significant digits.

TODO (NOW):
- fill form

TODO (2024):
- savings
- crypto to crypto trades (e.g. the BTC to DOT one)
- * -> 2024-03-15 (inclusive) I used USD to buy crypto, then I used RON (and some spare EUR and GBP)
- check if you cross any of the thresholds for CASS
"""

import os
import re
import sys

from collections import defaultdict, deque
from datetime import datetime, timedelta
from decimal import Decimal as D
from operator import itemgetter
from pathlib import Path
from pprint import pprint

from tqdm import tqdm
from dotenv import load_dotenv
from bs4 import BeautifulSoup

from katalytic.files import load, save, get_files

load_dotenv()


def main():
    rates_2022 = extract_exchange_rates(os.environ["EXCHANGE_RATES_2022"], 2022)
    rates_2023 = extract_exchange_rates(os.environ["EXCHANGE_RATES_2023"], 2023)
    rates = merge_rates(rates_2022, rates_2023)
    rates["USD"]["2023"] = "4.5743"  # Dividends use the yearly rate

    ibkr_dividends = extract_dividend_details(os.environ["IBKR_DIVIDENDS"])
    ibkr_dividends = process_revo(ibkr_dividends, rates)
    ibkr_dividends_profit = calc_profit(ibkr_dividends)

    ibkr_2022 = extract_ibkr_data(os.environ["IBKR_2022"])
    ibkr_2023 = extract_ibkr_data(os.environ["IBKR_2023"])
    ibkr = convert_ibkr_data_to_revolut_format(ibkr_2022 + ibkr_2023)

    ibkr = process_revo(ibkr, rates)
    ibkr_profit = calc_profit(ibkr)

    bcr_profit = D(os.environ["BCR"])
    stocks_profit = calc_profit_per_country(ibkr_profit, bcr_profit)

    if Path("tmp/revo.json").is_file():
        revo = load("tmp/revo.json")
    else:
        revo = extract_revo_data(os.environ["REVO"])
        save(revo, "tmp/revo.json")

    revo_2 = process_revo(revo, rates)
    revo_profit = calc_profit(revo_2)
    revo_profit = sort_dict_by_value(revo_profit)

    summary = generate_tax_summary(stocks_profit, ibkr_dividends_profit, revo_profit)
    pprint(summary)


def generate_tax_summary(stocks_by_country, ibkr_dividends, revo):
    stocks_by_country = {
        f"stocks:{country}": profit
        for country, profit in stocks_by_country.items()
        if profit != 0
    }
    summary = {
        **{k: (v, v * D(0.1)) for k, v in stocks_by_country.items()},
        "ibkr:dividends": (
            ibkr_dividends["__REWARDS__"],
            ibkr_dividends["__REWARDS__"] * D(0.08),
        ),
        "revo:rewards": (revo["__REWARDS__"], revo["__REWARDS__"] * D(0.1)),
        "revo:profit": (revo["__TOTAL__"], revo["__TOTAL__"] * D(0.1)),
    }
    return {k: (round(v1, 2), round(v2, 2)) for k, (v1, v2) in summary.items()}


def extract_dividend_details(path):
    html = load(path)
    soup = BeautifulSoup(html, "html.parser")
    table = soup.select_one("#tblDividendDetailBody .table-responsive table")

    dividends = []
    for row in table.find_all("tr"):
        cols = row.find_all("td")
        if len(cols) < 8:
            continue

        row = {
            "t_type": "Reward",
            # Dividends use the annual exchange rate. Remove the month and day so
            # it automatically picks the yearly rate later
            "date": cols[2].get_text(strip=True)[:4],
            "time": "00:00",
            "buy": cols[0].get_text(strip=True),
            "buy_Q": "1",
            "sell": "RON",
            "USD": cols[6].get_text(strip=True).split("<br>")[-1].strip(),
            "fee_%": None,
        }
        dividends.append(row)

    return dividends


def convert_ibkr_data_to_revolut_format(ibkr):
    revo_format = []
    for t in ibkr:
        t["total"] = t["total"].replace(",", "")
        t["quantity"] = t["quantity"].replace(",", "")
        if t["type"] == "buy":
            fee = "0.0"  # the commission is already included in the price
            total = t["total"]
            buy = t["symbol"]
            buy_Q = t["quantity"]
            sell = "RON"
            sell_Q = t["total"]
        else:
            # exclude the commission to match the revolut format
            commission = -D(t["commission"])  # remove the sign
            total = D(t["total"]) - commission
            fee = 100 * commission / total

            buy = "RON"
            buy_Q = str(total)
            sell = t["symbol"]
            sell_Q = t["quantity"]

        new = {
            "date": t["date"],
            "time": t["time"][:5],
            "t_type": t["type"].title(),
            "fee_%": str(fee).replace("-", ""),
            "USD": str(total).replace("-", ""),
            "buy": buy,
            "buy_Q": buy_Q.replace("-", ""),
            "sell": sell,
            "sell_Q": sell_Q.replace("-", ""),
        }

        revo_format.append(new)

    return revo_format


def calc_profit(trades):
    result = {}
    for coin, coin_trades in trades.items():
        if coin == "__COSTS__":
            result[coin] = -sum(ron for _, ron in coin_trades)
        elif coin == "__REWARDS__":
            rewards = {coin: sum(rewards) for coin, rewards in coin_trades.items()}
            rewards = sort_dict_by_value(rewards)
            result[coin] = sum(rewards.values())
        else:
            result[coin] = _calc_profit_for_coin(coin_trades)

    # Rewards count as income, not capital gains
    result["__TOTAL__"] = sum(n for coin, n in result.items() if coin != "__REWARDS__")
    return result


def _calc_profit_for_coin(trades):
    # The queue helps me find out if the coins to sell ever exceed the ones accumulated
    # up to that point. If I popleft on an empty queue, I'll get an error
    queue = deque()
    profit = 0

    for t in trades:
        if t[0] > 0:
            queue.append(t)
            continue

        coins_to_sell = -t[0]  # swap the sign to make the logic more intuitive
        RON_to_receive = t[1]
        while coins_to_sell:
            t2 = queue.popleft()
            coins_available = t2[0]
            RON_spent = -t2[1]  # swap the sign to make the logic more intuitive

            if coins_available == coins_to_sell:
                profit += RON_to_receive - RON_spent
                coins_to_sell = 0
            elif coins_available < coins_to_sell:
                RON_per_unit = RON_to_receive / coins_available
                RON_to_consume = coins_available * RON_per_unit
                RON_to_receive -= RON_to_consume

                profit += RON_to_consume - RON_spent
                coins_to_sell -= coins_available
            else:
                RON_per_unit = RON_spent / coins_available
                RON_to_consume = coins_to_sell * RON_per_unit
                RON_remaining = RON_to_consume - RON_spent

                profit += RON_to_receive - RON_to_consume
                coins_available -= coins_to_sell
                coins_to_sell = 0
                queue.appendleft((coins_available, RON_remaining))

    return profit


def process_revo(trades, rates):
    """
    Convert the USD value to RON because ANAF requires you to calculate it based
    on the BNR exchange rate for that day. Koinly might not follow this rule

    For the "Buy" trades I have to include the fee in the RON value, so it gets deducted
    from my taxes.

    Koinly uses UTC for the date. You don't have to convert it to local time.

    `ron_value` means how much I spent on the trade or how much the coins are worth at
    the time of the reward. I did not name it ron_change because for rewards, it doesn't
    change, but I need to know it for when I sell the crypto received through rewards.
    """
    new = defaultdict(list)
    rewards = defaultdict(list)
    for t in sort_by_datetime(trades):
        rate = D(rates["USD"][t["date"]])
        USD = D(t["USD"])

        if t["t_type"] == "Cost":
            assert not t["fee_%"], t
            coin = "__COSTS__"
            coin_change = 0
            ron_value = USD * rate
        elif t["t_type"] == "Reward":
            assert not t["fee_%"], t
            coin = t["buy"]
            coin_change = D(t["buy_Q"])
            ron_value = USD * rate
            rewards[coin].append(ron_value)
        elif t["sell"] == "RON":
            USD = USD / (1 - D(t["fee_%"]) / 100)
            coin = t["buy"]
            coin_change = D(t["buy_Q"])
            ron_value = -USD * rate
        elif t["buy"] == "RON":
            coin = t["sell"]
            coin_change = -D(t["sell_Q"])
            ron_value = USD * rate
        else:
            raise AssertionError(t)

        new[coin].append((coin_change, ron_value))

    new["__REWARDS__"] = rewards
    return dict(new)


def convert_to_RON(rows, rates):
    for row in rows:
        print(row["sell"], row["sell_Q"], row)
        d = D(row["USD"].replace(",", ""))
        rate = D(rates[row["date"]])
        row["RON"] = str(d * rate)
        if row["t_type"] == "Buy" and row["sell"] != "RON":
            print(row["sell"], row["sell_Q"], row)


def extract_exchange_rates(path, year):
    html = load(path)
    html = BeautifulSoup(html, "html.parser")
    html = html.find(id="Data_table")
    header = extract_texts(html.find("thead").find_all("th"))
    keys = ("USD", "EUR", "GBP")

    rates = defaultdict(dict)
    for row in html.find_all("tr")[1:]:
        row = extract_texts(row.find_all("div"))
        if len(row) == len(header):
            row = dict(zip(header, row))
            for k in keys:
                rates[k][row["Data"]] = row[k]

    # fill the gaps (e.g. weekends and holidays)
    prev = list(rates["USD"])[0]
    base = datetime(year, 1, 1)
    for i in range(365):
        date = (base + timedelta(days=i)).strftime("%Y-%m-%d")
        if date in rates["USD"]:
            prev = date
            continue

        for k in keys:
            rates[k][date] = rates[k][prev]

    return dict(rates)


def extract_ibkr_data(path):
    html = load(path)
    html = BeautifulSoup(html, "html.parser")
    html = html.find(id="summaryDetailTable")
    header = extract_texts(html.find("thead").find_all("th"))

    keys = {
        "Trade Date/Time": "date",
        "Type": "type",
        "Comm": "commission",
        "Price": "price",
        "Proceeds": "total",
        "Quantity": "quantity",
        "Symbol": "symbol",
    }

    rows = []
    for r in html.select("tr.row-summary"):
        row = extract_texts(r.find_all("td"))
        if len(row) == len(header):
            row = dict(zip(header, row))
            assert row["Fee"] == "0.00", row

            row = subdict(row, keys)
            row["currency"] = "USD"
            row["type"] = row["type"].lower()
            row["date"], row["time"] = row["date"].split(", ")

            rows.append(row)

    return rows


def extract_revo_data(path):
    trades = []
    for p in tqdm(get_files(path)[:]):
        html = load(p)

        html = BeautifulSoup(html, "html.parser")
        html = html.find(class_="well-list")

        date = None
        for element in html.find_all(["h5", "div"], recursive=False):
            if element.name == "h5":
                date = element.get_text(strip=True)
                date = datetime.strptime(date, "%b %d, %Y").strftime("%Y-%m-%d")
            elif element.name == "div" and "well" in element.get("class", []):
                assert date
                trade = revo_extract(element, date)
                if trade:
                    assert trade["t_type"] in ("Buy", "Sell", "Reward", "Cost"), trade
                    trades.append(trade)

    n_types = len([1 for x in trades if x["t_type"] is not None])
    n_dates = len([1 for x in trades if x["date"] is not None])

    n = len(trades)
    assert n == n_types, (n, n_types)
    assert n == n_dates, (n, n_dates)

    trades = rmap_strings(clean, trades)
    return sort_by_datetime(trades, reverse=True)


def sort_by_datetime(trades, reverse=False):
    return sorted(trades, key=itemgetter("date", "time"), reverse=reverse)


def clean(string):
    string = string.replace("≈", "")
    string = re.sub(r"\s{2,}", " ", string).strip()
    return string


def rmap_strings(f, data):
    if data is None:
        return ""
    elif isinstance(data, str):
        return f(data)
    elif isinstance(data, dict):
        return {k: rmap_strings(f, v) for k, v in data.items()}
    elif isinstance(data, (list, tuple)):
        return type(data)(rmap_strings(f, v) for v in data)
    else:
        return data


def revo_extract(div, date):
    sell_amount, sell_currency = revo_extract_sell(div)
    buy_amount, buy_currency = revo_extract_buy(div)

    t = {
        "date": date,
        "time": revo_extract_time(div),
        "t_type": revo_extract_t_type(div),
        "sell_Q": sell_amount,
        "sell": sell_currency,
        "buy_Q": buy_amount,
        "buy": buy_currency,
        "USD": revo_extract_usd_equivalent(div),
        "fee_%": revo_extract_fee(div),
        "cost_basis_$": revo_extract_cost_basis(div),
        "net_$": revo_extract_net(div),
    }

    assert t["date"], t
    assert t["time"], t
    assert t["USD"], t

    if t["t_type"] == "Cost":
        assert not t["buy"], t
        assert not t["buy_Q"], t
        assert t["sell"], t
        assert t["sell_Q"], t
        assert not t["fee_%"], t
        assert not t["net_$"], t
        assert not t["cost_basis_$"], t
    elif t["t_type"] == "Reward":
        assert t["buy"], t
        assert t["buy_Q"], t
        assert not t["sell"], t
        assert not t["sell_Q"], t
        assert not t["fee_%"], t
        assert not t["net_$"], t
        assert not t["cost_basis_$"], t
    elif t["sell"] == "RON":
        assert t["buy"], t
        assert t["buy_Q"], t
        assert t["sell"], t
        assert t["sell_Q"], t
        assert t["fee_%"], t
        assert not t["net_$"], t
        assert not t["cost_basis_$"], t
    elif t["buy"] == "RON":
        assert t["buy"], t
        assert t["buy_Q"], t
        assert t["sell"], t
        assert t["sell_Q"], t
        assert t["fee_%"], t
        assert t["cost_basis_$"], t

        # net_$ will be None when you sell a coin received as reward
        # and a number when you paid for at least part of it
        assert t["net_$"] or (t["net_$"] is None), t
    else:
        raise AssertionError(t)

    return t


def revo_extract_sell(div):
    selector = (
        "div.col.text-right.pr-2.pl-0-md.pr-0-md > "
        "div.media.align-items-center > "
        "div.media-body > "
        "div > "
        "span > "
        "div.wrap-ellipsis.small-xs.no-small-md"
    )
    element = div.select_one(selector)
    if element:
        text = element.text.strip().replace(",", "")
        if not text:
            return (None, None)

        text = re.sub(r"\.$", "", text)
        value = re.search(r"\d+(\.\d+)?", text)[0]
        currency = re.search(r"[A-Z]+", text)[0]
        return (value, currency)
    else:
        return (None, None)


def revo_extract_t_type(div):
    selector = (
        "div.col-4.col-md-2.p-0 > "
        "div.media.align-items-center.position-relative.d-inline-flex.z-1 > "
        "div.media-body.small-xs.no-small-md > "
        "div.text-nowrap"
    )
    element = div.select_one(selector)
    return element.text.strip()[:-1] if element else None


def revo_extract_time(div):
    selector = (
        "div.col-4.col-md-2.p-0 > "
        "div.media.align-items-center.position-relative.d-inline-flex.z-1 > "
        "div.media-body.small-xs.no-small-md > "
        "div:nth-child(2) > span > span"
    )
    element = div.select_one(selector)
    if element:
        return datetime.strptime(element.text.strip(), "%I:%M %p").strftime("%H:%M")
    else:
        return None


def revo_extract_buy(div):
    selector = (
        "div.col.pl-lg-0.pr-lg-0 > "
        "div.media.align-items-center > "
        "div.media-body > "
        "div > span > div.wrap-ellipsis.small-xs.no-small-md"
    )
    element = div.select_one(selector)
    if element:
        text = element.text.strip().replace(",", "")
        if not text:
            return (None, None)

        text = re.sub(r"\.$", "", text)
        value = re.search(r"\d+(\.\d+)?", text)[0]
        currency = re.search(r"[A-Z]+", text)[0]
        return (value, currency)
    else:
        return (None, None)


def revo_extract_fee(div):
    selector = (
        "div.col-12.col-sm-3.col-md-2.p-0.text-right.text-nowrap > "
        "div.d-flex.justify-content-end.align-items-center > "
        "span > span.bg-fee-box.badge.badge-pill.text-muted"
    )
    element = div.select_one(selector)
    if element:
        return element.text.strip().replace("% fee", "")
    else:
        return None


def revo_extract_usd_equivalent(div):
    selector = (
        "div.col.pl-lg-0.pr-lg-0 > "
        "div.media.align-items-center > "
        "div.media-body > "
        "div.text-muted.small.mb-1 > span > span.rounded.p-1.editable-bg"
    )
    element = div.select_one(selector)
    if element:
        return element.text.replace("$", "").replace("≈ ", "").strip()
    else:
        return None


def revo_extract_cost_basis(div):
    selector = (
        "div.col.text-right.pr-2.pl-0-md.pr-0-md > "
        "div.media.align-items-center > "
        "div.media-body > "
        "div.text-muted.small.mb-1 > span > span"
    )
    element = div.select_one(selector)
    if element:
        text = element.text
        text = text.replace("cost basis", "")
        text = text.replace("$", "")
        text = text.replace(",", "")
        return text.strip()
    else:
        return None


def revo_extract_net(div):
    selector = (
        "div.col.pl-lg-0.pr-lg-0 > "
        "div.media.align-items-center > "
        "div.media-body > "
        "div.text-muted.small.mb-1 > span:nth-child(3) > b > span"
    )
    element = div.select_one(selector)
    if element:
        text = element.text
        text = re.sub("profit|loss", "", text)
        text = text.replace("$", "")
        text = text.replace(",", "")
        return text.strip()
    else:
        return None


def calc_profit_per_country(ibkr_profit, bcr_profit):
    countries = {
        "AI": "US",
        "AMD": "US",
        "AQST": "US",
        "ARM": "UK",
        "CTMX": "US",
        "GOOGL": "US",
        "H2O": "RO",
        "INTC": "US",
        "MSFT": "US",
        "PBYI": "US",
        "PIXY": "US",
        "PLTR": "US",
        "RBLX": "US",
        "SYRS": "US",
        "TSLA": "US",
        "U": "US",
        "UPWK": "US",
        "VFS": "Vietnam",
    }

    profit_per_country = defaultdict(D)
    profit_per_country["RO"] = bcr_profit

    for currency, profit in ibkr_profit.items():
        if currency.startswith("__"):
            continue

        country = countries[currency]
        profit_per_country[country] += profit

    return dict(profit_per_country)


def extract_texts(children):
    return [c.text.strip() for c in children if c.name is not None]


def subdict(d, keys):
    if isinstance(keys, dict):
        return {k: d[k_old] for k_old, k in keys.items()}
    else:
        return {k: d[k] for k in keys}


def merge_rates(rates_1, rates_2):
    return {
        currency: {**mapping, **rates_2[currency]}
        for currency, mapping in rates_1.items()
    }


def sort_dict_by_value(d, reverse=False):
    return dict(sorted(d.items(), key=itemgetter(1), reverse=reverse))


if __name__ == "__main__":
    main()

