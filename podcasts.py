import re
from pathlib import Path

from boltons.strutils import asciify
from katalytic.files import get_files, move_file
from titlecase import titlecase


def main():
    for f in get_files("/media/wip/2TB/downloads/0_podcasts", recursive=True):
        if not f.endswith(".mp3"):
            continue

        fn = str(Path(f).stem)
        new_fn = asciify(fn, ignore=True).decode("utf-8")
        new_fn = new_fn.replace('"', "")
        new_fn = new_fn.replace("|", "-")
        new_fn = new_fn.replace(":", " -")
        new_fn = new_fn.replace("?", "")
        new_fn = new_fn.replace("!", "")
        new_fn = new_fn.replace("*", "_")
        new_fn = re.sub(r"'\w+", lambda m: m.group(0).lower(), new_fn)
        new_fn = titlecase(new_fn)

        # do not change the casing of youtube video IDs
        m = re.search(r"( - [a-zA-Z0-9_-]{11})\.mp3$", f)
        if m:
            m = m.group(1)
            n = len(m)
            new_fn = f"{new_fn[:-n]}{m}"

        new_fn_2 = (
            modern_wisdom(new_fn)
            or tim_ferriss(new_fn)
            or andrew_huberman(new_fn)
            or joe_rogan(new_fn)
            or lex_fridman(new_fn)
            or hoe_math(new_fn)
        )
        new_fn = new_fn_2 or new_fn

        ext = str(Path(f).suffix)
        new_f = str(Path(f).with_name(new_fn + ext))
        new_f = re.sub(r"\s{2,}", " ", new_f)
        new_f = re.sub(r"- ;(\w)", r"- \1", new_f)
        if "/NA/" in new_f:
            new_f = new_f.replace("/NA/", "/")

        if m and m not in new_f:
            new_f = new_f.replace(".mp3", f"{m}.mp3")

        if new_f != f:
            print(f)
            print(new_f)
            print()
            move_file(f, new_f)


def hoe_math(fn):
    m = re.search(r"Live Q&A (\d{4}) (\d{2}) (\d{2})", fn)
    if m:
        year, month, day = m.groups()
        return f"Hoe Math - {year}-{month}-{day}"

    m = re.search(r"Live Q&A (\d{2}) (\d{2}) (\d{4})", fn)
    if m:
        month, day, year = m.groups()
        return f"Hoe Math - {year}-{month}-{day}"


def lex_fridman(fn):
    m = re.search(r"(.*) - (.*) - (.*) - Lex Fridman Podcast #(\d+)", fn)
    if m:
        host, guest, title, episode = m.groups()
        return f"{host}, {guest} - {episode} - {title}"

    m = re.search(r"(.*) - (.*) - Lex Fridman Podcast #(\d+)", fn)
    if m:
        host, title, episode = m.groups()
        return f"{host} - {episode} - {title}"


def joe_rogan(fn):
    m = re.search(r"(.*) W?_(.+) _ Joe Rogan", fn)
    if m:
        title, guest = m.groups()
        return f"Joe Rogan, {guest} - {title}"

    m = re.search(r"(.*) The Joe Rogan Experience", fn)
    if m:
        title = m.group(1)
        return f"Joe Rogan - {title}"

    m = re.search(r"Joe Rogan Experience #(\d+) - (.*)", fn)
    if m:
        episode, guest = m.groups()
        guest = guest.split("-")
        if any(c.isdigit() for c in guest[-1]):
            guest = "-".join(guest[:-1])  # probably the youtube video id
        else:
            guest = "-".join(guest)

        return f"Joe Rogan, {guest} - {episode}"


def andrew_huberman(fn):
    m = re.search(r"(.*) - (.*) [-_] Huberman Lab Podcast #(\d+)", fn)
    if m:
        guest, title, episode = m.groups()
        return f"Andrew Huberman, {guest} - {episode} - {title}"

    m = re.search(r"(.*) - (.*) [-_] Huberman Lab Guest Series", fn)
    if m:
        guest, title = m.groups()
        return f"Andrew Huberman, {guest} - {title}"

    m = re.search(r"(.*) [-_] Huberman Lab Podcast", fn)
    if m:
        title = m.group(1)
        return f"Andrew Huberman - {title}"

    return None


def modern_wisdom(fn):
    m = re.search(r"(.*) - (.*) - Modern Wisdom.* (\d+)", fn)
    if not m:
        return None

    title, guest, episode = m.groups()
    return f"Modern Wisdom, {guest} - {episode} - {title}"


def tim_ferriss(fn):
    m = re.search(r"(\d+) - (.*)  (.*) _ the Tim Ferriss.*", fn)
    if m:
        episode, guest, title = m.groups()
        title = title[0].upper() + title[1:]
        return f"Tim Ferriss, {guest} - {episode} - {title}"

    m = re.search(r"(\d+) - Books I've Loved\s*(.*\S)\s+_ the Tim Ferriss.*", fn)
    if m:
        episode, guest = m.groups()
        if "Books" in guest:
            return f"Tim Ferriss - {episode} - Books I've Loved - {guest}"
        else:
            return f"Tim Ferriss, {guest} - {episode} - Books I've Loved"


if __name__ == "__main__":
    main()
