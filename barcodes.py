import os
import shutil

from barcode import EAN13
from barcode.writer import SVGWriter
import svgwrite
import re
import cairosvg
from pathlib import Path
from pprint import pprint


def load_csv(path):
    with open(path, "r") as f:
        return [line.strip().split(",")[:2] for line in f]


def main():
    shutil.rmtree('barcodes_svg', ignore_errors=True)
    shutil.rmtree('barcodes_png', ignore_errors=True)
    Path("barcodes_svg").mkdir(exist_ok=True)
    Path("barcodes_png").mkdir(exist_ok=True)

    dpi = 300
    for j in range(1, 9):
        path_csv = f"Kopie von Gel-polish Barcodes - Gel polish #{j}.csv"
        print(path_csv)
        barcodes = load_csv(path_csv)
        for i, (item, barcode) in enumerate(barcodes, start=1):
            if not barcode:
                continue
            elif barcode == 'Barcode / EAN-13':
                continue

            path = f'barcodes_svg/{i}_{item}'
            path_svg = f"{path}.svg"
            path_png = path_svg.replace("svg", "png")
            if Path(path_svg).exists():
                print(f"Skipping {path_svg}")
                break

            generate_ean13_barcode(barcode, path, dpi=dpi)
            change_color_and_remove_the_angle_bracket(path_svg, "black", "#FFF7F1")
            cairosvg.svg2png(url=path_svg, write_to=path_png, dpi=dpi)


def change_color_and_remove_the_angle_bracket(path, before, after):
    with open(path, "r") as f:
        svg = f.read()

    svg = re.sub(".*rect width.*\n", "", svg)  # remove white background
    svg = re.sub(".*&gt;.*\n", "", svg)  # remove angle bracket
    svg = svg.replace(before, after)  # change color

    with open(path, "w") as f:
        f.write(svg)


def generate_ean13_barcode(barcode, path, dpi=300):
    writer = SVGWriter()
    writer.dpi = 300
    ean = EAN13(barcode, writer=writer, guardbar=True)
    ean.save(path)


if __name__ == "__main__":
    main()

