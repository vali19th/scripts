import re
import sys
from collections import Counter
from pathlib import Path
from pprint import pformat

from katalytic.files import get_files, move_file


def main():
    for old_path, new_path in map_filenames_to_numbers(get_files(sys.argv[1])).items():
        if old_path != new_path:
            print(f"rename({old_path!r}, {new_path!r})")
            move_file(old_path, new_path)


def map_filenames_to_numbers(paths):
    """The paths that already have numerical IDs will maintain their IDs"""
    items_and_ids = tuple((item, re.search(r"^(\d+)\.\w[.a-zA-Z0-9]*$", Path(item).name)) for item in paths)
    used_ids = [int(id.group(1)) for _, id in items_and_ids if id]
    rename_mapping = {item: item for item, id in items_and_ids if id}
    if len(rename_mapping) == len(paths):
        return rename_mapping

    conflicts = {id: n for id, n in Counter(used_ids).items() if n >= 2}
    if conflicts:
        raise ValueError(f"There are multiple files with the same numerical id: {pformat(conflicts)}")

    i = min(i for i in range(1, len(paths) + 1) if i not in used_ids)
    for item in paths:
        if item not in rename_mapping:
            ext = "".join(Path(item).suffixes)
            rename_mapping[item] = f"{Path(item).parent}/{i}{ext}"
            used_ids.append(i)
            i = min(i for i in range(i, len(paths) + 2) if i not in used_ids)

    return rename_mapping


if __name__ == "__main__":
    main()
